/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// force the download of a link.
// based on the html5 download tag,
// so it might not work in other browsers
var start_download = function(url) {
	var a = document.createElement("a");
	a.href = url;
	a.download = true;
	a.style.dispaly = "none";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
};

// register a new shortcut to be displayed on the focus page
var fifepx_add_shortcut = function(key, callback) {
	// find the ul containing the shortcuts
	var ul = document.getElementById("focus_keyboard").getElementsByTagName("ul")[0];

	// create the new shortcut
	var li = document.createElement("li");
	var a = document.createElement("a");
	var span = document.createElement("span");
	a.className = "key";
	a.innerHTML = key.toUpperCase().charAt(0);
	span.className = "label";
	span.innerHTML = "Download";
	li.appendChild(a);
	li.appendChild(span);

	// prepend the new shortcut to the list
	ul.insertBefore(li, ul.firstChild);

	// register the key listener
	document.body.addEventListener("keypress", function(e) {
		if(e.keyCode == key.toLowerCase().charCodeAt(0)) {
			callback(e);
		}
	});
};

// register the download shortcut
// extract image url and force the download
fifepx_add_shortcut("D", function(e) {
	var id = window.location.pathname.split("/")[2];
	var src = document.getElementById("photo_" + id).getElementsByTagName("img")[0].src;
	start_download(src);
});
